from django.shortcuts import render
from django.http import HttpResponse, Http404

from .models import ReportPage

from config.constants import NACIONAL, REGIONES


def find_report_dynamically_view(request, the_slug):
    r = ReportPage.objects.get(slug=the_slug)

    if r.merge_Lima_Region_and_Lima_Metropolitana == True:
        # Merge regiones by deleting Lima Metropolitana (id 15)
        # and modifying the capital and name of Lima Región (id 14)
        regiones = REGIONES.copy()
        del regiones[15]
        regiones[14]['capital'] = 'Lima'
        regiones[14]['nombre'] = 'Lima'
    else:
        regiones = REGIONES

    return render(
        request,
        r.html_report_template,
        {
            "report_page": r,
            "REGIONES": regiones,
            "NACIONAL": NACIONAL,
        },
    )


def find_region_dynamically_view(request, region_name):
    region = [
        r for r in REGIONES if r["nombre"] == str(region_name)
    ]
    if len(region) != 1:
        raise Http404()
    return render(request, "reporte_por_region.html", {"region": region[0]})


def nacional_view(request):
    return render(request, "reporte_nacional.html", {"nacional": NACIONAL})
