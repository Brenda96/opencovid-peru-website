"""opencovid URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from home.views import home_view, team_view

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', home_view),
    path('team/', team_view, name='team_view'),
    path('reportes/', include('reportes.urls')),
    path('regiones/', include('reportes.regions_urls')),
    path('nacional/', include('reportes.nacional_urls')),
    path('tinymce/', include('tinymce.urls')),
    path('^filer/', include('filer.urls')),
]

if settings.DEBUG:
    if "debug_toolbar" in settings.INSTALLED_APPS:
        import debug_toolbar
        urlpatterns = \
            [path("__debug__/", include(debug_toolbar.urls))] + urlpatterns
    
    if settings.STORAGE_MODE == 'local':
        urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

admin.site.site_header = 'OpenCovid-Perú Admin'
admin.site.site_title = 'OpenCovid-Perú Administration'
admin.site.index_title = 'Welcome to OpenCovid-Perú Administration'
